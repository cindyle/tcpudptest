package com.example.tcpudptest;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;

public class ClientActivity extends AppCompatActivity {
    public static final String TAG = "TAG";
    private EditText editText, edt_ip;
    private Button btn_send, btn_ip;
    private TextView tv_history;
    private Socket socket;
    private String ip, msg;
    private Handler handler = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);
        findViews();
        btn_ip.setOnClickListener(ipClick);
        btn_send.setOnClickListener(sendClick);
    }

    private View.OnClickListener ipClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ip = edt_ip.getText().toString();
            Log.i(TAG, "get ip ok , ip : " + ip);

            Thread thread = new Thread(connectServer);
            thread.start();
        }
    };

    private View.OnClickListener sendClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            msg = editText.getText().toString();
            tv_history.append(msg + "\n");
            editText.setText("");

            Thread sendMsgThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    if (socket.isConnected()) {
                        // 傳送訊息
                        try {
                            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                            bufferedWriter.write(msg);
                            bufferedWriter.flush();
                            Log.i(TAG, "msg :" + msg);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }
            });
            sendMsgThread.start();


        }
    };


    private Runnable connectServer = new Runnable(){
        @Override
        public void run() {
            try {
                socket = new Socket(ip, 8888);
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                // 接收訊息
                while (socket.isConnected()) {
                    Log.i(TAG, "connect server success" );
                    final String serverMsg = bufferedReader.readLine();
                    if(serverMsg != null) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                tv_history.append(serverMsg + "\n");
                                Log.i("TAG", "server msg_tv: " + serverMsg);
                            }
                        });
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };


    private void findViews() {
        editText = findViewById(R.id.editText);
        edt_ip = findViewById(R.id.edt_ip);
        btn_send = findViewById(R.id.btn_send);
        btn_ip = findViewById(R.id.btn_ip);
        tv_history = findViewById(R.id.tv_history);
    }
}
