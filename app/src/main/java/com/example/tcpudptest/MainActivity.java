package com.example.tcpudptest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button btn_server, btn_client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViews();
        btn_server.setOnClickListener(serverClick);
        btn_client.setOnClickListener(clientClick);

    }

    private View.OnClickListener serverClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setClass(MainActivity.this, ServerActivity.class);
            startActivity(intent);

        }
    };

    private View.OnClickListener clientClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setClass(MainActivity.this, ClientActivity.class);
            startActivity(intent);

        }
    };

    private void findViews() {
        btn_server = findViewById(R.id.btn_server);
        btn_client = findViewById(R.id.btn_client);
    }
}
