package com.example.tcpudptest;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.ArrayList;

public class ServerActivity extends AppCompatActivity {
    public static final String TAG = "TAG";
    private EditText editText;
    private Button btn_send;
    private TextView tv_history, tv_ip;
    private ServerSocket serverSocket;
    private Socket socket;
    private Handler handler = new Handler();
    private String msg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server);
        findViews();
        getServerIp();

        Thread thread = new Thread(serverThread);
        thread.start();

        btn_send.setOnClickListener(listener);

    }

    private Runnable serverThread = new Runnable() {
        @Override
        public void run() {
            try {
                // 建立serverSocket
                serverSocket = new ServerSocket(8888);
                Log.i(TAG, "server is start.");

                // 等待連線
                while (true) {
                    // 接收連線
                    socket = serverSocket.accept();
                    Log.i(TAG, "server is waiting for client connect.");
                    while (socket.isConnected()) {
                        Log.i(TAG, "server is connected.");
                        // 接收訊息
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                        while (socket.isConnected()) {
                            final String serverMsg = bufferedReader.readLine();
                            if (serverMsg != null ) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        tv_history.append(serverMsg);
                                    }
                                });
                            }
                        }
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            msg = editText.getText().toString();
            tv_history.append(msg + "\n");
            editText.setText("");
            final Thread sendMsgThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    // 傳送訊息
                    if (socket.isConnected()) {
                        try {
                            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                            bufferedWriter.write(msg);
                            bufferedWriter.flush();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            sendMsgThread.start();

        }
    };


    private void getMessage(Socket socket) {
        try {
            // 接收訊息
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            while (socket.isConnected()) {
                final String serverMsg = bufferedReader.readLine();
                if (serverMsg != null ) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            tv_history.append(serverMsg);
                        }
                    });
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }








    }


    public String getServerIp() {
        // 新增一個wifiManager物件並取得wifi_service
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        // 取得wifi資訊
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        // 取得ip（取回之ip需經過換算）
        int ipAddress = wifiInfo.getIpAddress();
        // 利用位移運算和AND運算計算ip
        String ip = String.format("%d.%d.%d.%d",(ipAddress & 0xff),(ipAddress >> 8 & 0xff),(ipAddress >> 16 & 0xff),(ipAddress >> 24 & 0xff));
        tv_ip.setText(ip);
        return ip;
    }

    private void findViews() {
        editText = findViewById(R.id.editText);
        btn_send = findViewById(R.id.btn_send);
        tv_history = findViewById(R.id.tv_history);
        tv_ip = findViewById(R.id.tv_ip);
    }
}
